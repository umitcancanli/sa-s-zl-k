﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApplication2.Models
{
    public class Uye
    {
        public int UyeID { get; set; }
        public string Rol { get; set; }
        [MaxLength(25, ErrorMessage = "kullanıcı adı 3-25 karakter aralığında olmalıdır.")]
        [MinLength(3, ErrorMessage = "kullanıcı adı 3-25 karakter aralığında olmalıdır.")]
        [Required(ErrorMessage = "kullanıcı adı gereklidir.")]
        public string Kullanici_adi { get; set; }
        [MaxLength(50, ErrorMessage = "şifre 6-50 karakter aralığında olmalıdır.")]
        [MinLength(6, ErrorMessage = "şifre 6-50 karakter aralığında olmalıdır.")]
        [Required(ErrorMessage = "şifre gereklidir.")]
        public string Sifre { get; set; }
        [EmailAddress(ErrorMessage = "girmiş olduğunuz e-posta adresi geçerli değildir.")]
        [Required(ErrorMessage ="e-posta adresi gereklidir.")]
        public string E_posta { get; set; }
        public char Cinsiyet { get; set; }
        [Range(1, 31, ErrorMessage = "gün 1-31 aralığında olmalıdır.")]
        [Required(ErrorMessage = "doğum tarihi gereklidir.")]
        public int Dogum_tarihi_gun { get; set; }
        [Range(1,12, ErrorMessage = "ay 1-12 aralığında olmalıdır.")]
        [Required(ErrorMessage = "doğum tarihi gereklidir.")]
        public int Dogum_tarihi_ay { get; set; }
        [Range(1900, 2016, ErrorMessage = "yıl 1900-2016 aralığında olmalıdır.")]
        [Required(ErrorMessage = "doğum tarihi gereklidir.")]
        public int Dogum_tarihi_yil { get; set; }
        public ICollection<Entry> Entryler { get; set; }

    }
}