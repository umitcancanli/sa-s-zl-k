﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Baslik
    {
        public int BaslikID { get; set; }
        public string Icerik { get; set; }
        public string Kategori { get; set; }
        public ICollection<Entry> Entryler { get; set; }

    }
}