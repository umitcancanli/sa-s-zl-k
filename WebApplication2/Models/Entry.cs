﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Entry
    {
        public int EntryID { get; set; }
        public string Icerik { get; set; }
        public string Yazar { get; set; }
        public DateTime Tarih { get; set; }
        public DateTime? Duzenleme_tarihi { get; set; }
        public int Oy { get; set; }
        public int Paylasim { get; set; }
        public string Oylayanlar { get; set; }

    }
}