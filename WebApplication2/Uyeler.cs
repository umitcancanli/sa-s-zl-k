//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public partial class Uyeler
    {
        public Uyeler()
        {
            rol = "yazar";
        }
        public int id { get; set; }
        public string rol { get; set; }
        [MaxLength(25, ErrorMessage = "kullan�c� ad� 3-25 karakter aral���nda olmal�d�r.")]
        [MinLength(3, ErrorMessage = "kullan�c� ad� 3-25 karakter aral���nda olmal�d�r.")]
        [Required(ErrorMessage = "kullan�c� ad� gereklidir.")]
        [Remote("KullaniciAdiKontrol", "Home", HttpMethod = "POST", ErrorMessage = "kullan�c� ad� kullan�mdad�r. l�tfen farkl� bir kullan�c� ad� giriniz.")]
        public string kullaniciAdi { get; set; }
        [MaxLength(50, ErrorMessage = "�ifre 6-50 karakter aral���nda olmal�d�r.")]
        [MinLength(6, ErrorMessage = "�ifre 6-50 karakter aral���nda olmal�d�r.")]
        [Required(ErrorMessage = "�ifre gereklidir.")]
        public string sifre { get; set; }
        [EmailAddress(ErrorMessage = "girmi� oldu�unuz e-posta adresi ge�erli de�ildir.")]
        [Required(ErrorMessage = "e-posta adresi gereklidir.")]
        [Remote("ePostaKontrol", "Home", HttpMethod = "POST", ErrorMessage = "girmi� oldu�unuz e-posta adresi ile bir kay�t mevcuttur.")]
        public string ePosta { get; set; }
        public string cinsiyet { get; set; }
        [Range(1, 31, ErrorMessage = "g�n 1-31 aral���nda olmal�d�r.")]
        [Required(ErrorMessage = "do�um tarihi gereklidir.")]
        public Nullable<int> dTarihGun { get; set; }
        [Range(1, 12, ErrorMessage = "ay 1-12 aral���nda olmal�d�r.")]
        [Required(ErrorMessage = "do�um tarihi gereklidir.")]
        public Nullable<int> dTarihAy { get; set; }
        [Range(1900, 2016, ErrorMessage = "y�l 1900-2016 aral���nda olmal�d�r.")]
        [Required(ErrorMessage = "do�um tarihi gereklidir.")]
        public Nullable<int> dTarihYil { get; set; }
    }
}
