//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Iletisim
    {
        public int id { get; set; }
        [Required]
        [EmailAddress]
        public string EPosta { get; set; }
        [MinLength(1, ErrorMessage = "bo� mesaj g�nderemezsiniz.")]
        public string mesaj { get; set; }
    }
}
