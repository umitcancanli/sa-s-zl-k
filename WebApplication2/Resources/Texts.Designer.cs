﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication2.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Texts {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Texts() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("WebApplication2.Resources.Texts", typeof(Texts).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Başlık ya da Yazar Ara.
        /// </summary>
        public static string Ara {
            get {
                return ResourceManager.GetString("Ara", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ay.
        /// </summary>
        public static string Ay {
            get {
                return ResourceManager.GetString("Ay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ayarlar.
        /// </summary>
        public static string Ayarlar {
            get {
                return ResourceManager.GetString("Ayarlar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ben.
        /// </summary>
        public static string Ben {
            get {
                return ResourceManager.GetString("Ben", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bilim.
        /// </summary>
        public static string Bilim {
            get {
                return ResourceManager.GetString("Bilim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Diğer.
        /// </summary>
        public static string Cdiğer {
            get {
                return ResourceManager.GetString("Cdiğer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Çıkış.
        /// </summary>
        public static string Çıkış {
            get {
                return ResourceManager.GetString("Çıkış", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cinsiyet.
        /// </summary>
        public static string Cinsiyet {
            get {
                return ResourceManager.GetString("Cinsiyet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Diğer.
        /// </summary>
        public static string Diğer {
            get {
                return ResourceManager.GetString("Diğer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Doğum Tarihi.
        /// </summary>
        public static string DoğumTarihi {
            get {
                return ResourceManager.GetString("DoğumTarihi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Eğitim.
        /// </summary>
        public static string Eğitim {
            get {
                return ResourceManager.GetString("Eğitim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to E-Posta.
        /// </summary>
        public static string EPosta {
            get {
                return ResourceManager.GetString("EPosta", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Erkek.
        /// </summary>
        public static string Erkek {
            get {
                return ResourceManager.GetString("Erkek", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Giriş.
        /// </summary>
        public static string Giriş {
            get {
                return ResourceManager.GetString("Giriş", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Giriş Yap.
        /// </summary>
        public static string GirişYap {
            get {
                return ResourceManager.GetString("GirişYap", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gönder.
        /// </summary>
        public static string Gönder {
            get {
                return ResourceManager.GetString("Gönder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gün.
        /// </summary>
        public static string Gün {
            get {
                return ResourceManager.GetString("Gün", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gündem.
        /// </summary>
        public static string Gündem {
            get {
                return ResourceManager.GetString("Gündem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to hoşgeldin.
        /// </summary>
        public static string hoşgeldin {
            get {
                return ResourceManager.GetString("hoşgeldin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İletişim.
        /// </summary>
        public static string İletişim {
            get {
                return ResourceManager.GetString("İletişim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İstatistikler.
        /// </summary>
        public static string İstatistikler {
            get {
                return ResourceManager.GetString("İstatistikler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kadın.
        /// </summary>
        public static string Kadın {
            get {
                return ResourceManager.GetString("Kadın", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kategoriler.
        /// </summary>
        public static string Kategoriler {
            get {
                return ResourceManager.GetString("Kategoriler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kayıt.
        /// </summary>
        public static string Kayıt {
            get {
                return ResourceManager.GetString("Kayıt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kayıt Ol.
        /// </summary>
        public static string KayıtOl {
            get {
                return ResourceManager.GetString("KayıtOl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kullanıcı Adı.
        /// </summary>
        public static string KullanıcıAdı {
            get {
                return ResourceManager.GetString("KullanıcıAdı", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mesajınız.
        /// </summary>
        public static string Mesaj {
            get {
                return ResourceManager.GetString("Mesaj", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mesajlar.
        /// </summary>
        public static string Mesajlar {
            get {
                return ResourceManager.GetString("Mesajlar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Müzik.
        /// </summary>
        public static string Müzik {
            get {
                return ResourceManager.GetString("Müzik", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rastgele.
        /// </summary>
        public static string Rastgele {
            get {
                return ResourceManager.GetString("Rastgele", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Şifre.
        /// </summary>
        public static string Şifre {
            get {
                return ResourceManager.GetString("Şifre", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Şifremi Unuttum.
        /// </summary>
        public static string ŞifremiUnuttum {
            get {
                return ResourceManager.GetString("ŞifremiUnuttum", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sinema.
        /// </summary>
        public static string Sinema {
            get {
                return ResourceManager.GetString("Sinema", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Spor.
        /// </summary>
        public static string Spor {
            get {
                return ResourceManager.GetString("Spor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sıkça Sorulan Sorular.
        /// </summary>
        public static string SSS {
            get {
                return ResourceManager.GetString("SSS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Teknoloji.
        /// </summary>
        public static string Teknoloji {
            get {
                return ResourceManager.GetString("Teknoloji", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeniler.
        /// </summary>
        public static string Yeniler {
            get {
                return ResourceManager.GetString("Yeniler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yıl.
        /// </summary>
        public static string Yıl {
            get {
                return ResourceManager.GetString("Yıl", resourceCulture);
            }
        }
    }
}
