﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Globalization;
using System.Threading.Tasks;
using WebApplication2.Models;
using System.Data.Entity.Validation;
using System.Web.Security;
using System.Data.SqlClient;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        private SozlukContext db = new SozlukContext();
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult cikis()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("index");
        }

        public ActionResult Yeniler()
        {
            return View(db.Entryler.ToList());
        }
        public ActionResult giris()
        {
            return View();
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult giris(Uyeler uye)
        {
            using (db)
            {
                var v = db.Uyeler.Where(k => k.kullaniciAdi.Equals(uye.kullaniciAdi) && k.sifre.Equals(uye.sifre)).FirstOrDefault();
                if (v != null)
                {
                    Session["UserID"] = v.id.ToString();
                    Session["Username"] = v.kullaniciAdi.ToString();
                    Session["Rol"] = v.rol.ToString();
                    FormsAuthentication.SetAuthCookie(uye.kullaniciAdi, false);
                    if (v.rol.ToString() == "admin")
                    {
                        ViewBag.rol = "admin";
                        return RedirectToAction("AdminPanel");
                    }
                    return RedirectToAction("AfterLogin");
                }
                else
                {
                    ModelState.AddModelError("","kullanıcı adı ya da şifre hatalı");
                }
            }
            return View();
        }

        [Authorize]
        public ActionResult AfterLogin()
        {
            return View("Gir");
        }

        [HttpGet]
        public ActionResult Kayit()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Kayit([Bind(Include = "id,rol,kullaniciAdi,sifre,ePosta,cinsiyet,dTarihGun,dTarihAy,dTarihYil")] Uyeler uye)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Uyeler.Add(uye);
                    db.SaveChanges();
                    return RedirectToAction("giris");
                }
                catch (DbEntityValidationException e)
                {

                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Response.Write(string.Format("Entity türü \"{0}\" şu hatalara sahip \"{1}\" Geçerlilik hataları:", eve.Entry.Entity.GetType().Name, eve.Entry.State));
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Response.Write(string.Format("- Özellik: \"{0}\", Hata: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                        }
                        Response.End();
                    }
                }
            }
            return View();
        }

        [HttpPost]
        public JsonResult KullaniciAdiKontrol(string kullaniciAdi)
        {
            return Json(!db.Uyeler.Any(x => x.kullaniciAdi == kullaniciAdi), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ePostaKontrol(string ePosta)
        {
            return Json(!db.Uyeler.Any(x => x.ePosta == ePosta), JsonRequestBehavior.AllowGet);
        }

        public ActionResult iletisim()
        {
            return View();
        }
        [HttpPost]
        public ActionResult iletisim([Bind(Include = "ePosta, mesaj")] Iletisim iletisim)
        {
            if (ModelState.IsValid)
            {
                db.Iletisim.Add(iletisim);
                db.SaveChanges();
                return RedirectToAction("MesajIletim");
            }
            return View();
        }
        public ActionResult MesajIletim()
        {
            return View();
        }
        public ActionResult istatistikler()
        {
            return View();
        }
        public ActionResult Ara(string ara)
        {
            var b = db.Basliklar.Where(x => x.baslik.Equals(ara));
            if (b != null)
            {
                return RedirectToAction("Baslik", new { baslik = ara });
            }
            else
            {
                return RedirectToAction("YeniBaslik", new { baslik = ara });
            }
        }

        public ActionResult Baslik(string baslik)
        {
            TempData["baslik"] = baslik;
            var e = db.Entryler.Where(x => x.Baslik.Equals(baslik)).ToList();
            return View(e);
        }

        [Authorize]
        public ActionResult YeniBaslik(string baslik)
        {
            TempData["baslik"] = baslik;
            return View();
        }

        [HttpPost]
        public ActionResult EntryGir(string entry)
        {
            Entryler Entry = new Entryler();
            Entry.Entry = entry;
            Entry.Yazar = Session["Username"].ToString();
            Entry.Tarih = DateTime.Now;
            Entry.Oy = 0;
            try
            {
                db.Entryler.Add(Entry);
                db.SaveChanges();
                return RedirectToAction("index");
            }
            catch (DbEntityValidationException e)
            {

                foreach (var eve in e.EntityValidationErrors)
                {
                    Response.Write(string.Format("Entity türü \"{0}\" şu hatalara sahip \"{1}\" Geçerlilik hataları:", eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Response.Write(string.Format("- Özellik: \"{0}\", Hata: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                    Response.End();
                }
            }
            return View();
        }

        [Authorize]
        public ActionResult AdminPanel()
        {
            return View(db.Entryler.ToList());
        }

        [Authorize]
        public ActionResult AdminPanelYazarlar()
        {
            return View(db.Uyeler.ToList());
        }

        public ActionResult YazarSil(int id)
        {
            Uyeler uye = db.Uyeler.Find(id);
            db.Uyeler.Remove(uye);
            db.SaveChanges();
            return RedirectToAction("AdminPanelYazarlar");
        }

        public ActionResult EntrySil(int id)
        {
            Entryler entry = db.Entryler.Find(id);
            db.Entryler.Remove(entry);
            db.SaveChanges();
            return RedirectToAction("AdminPanel");
        }
        public ActionResult Dil(string dil)
        {
            if (dil != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(dil);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(dil);
            }

            HttpCookie cookie = new HttpCookie("Dil");
            cookie.Value = dil;
            Response.Cookies.Add(cookie);

            return View("Index");
        }
    }
}